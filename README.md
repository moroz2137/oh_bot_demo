# OMise &mdash; client management demo

This is a simple application developed for the purposes of job recruitment.
The application is built using Node.js + Express.js + MongoDB + Vue.js stack.

## Up and running

The application requires Node.js, MongoDB, and Yarn to run properly.

```shell
yarn install
yarn start
```
