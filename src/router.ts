import Express from "express";
import PageController from "./controllers/PageController";
import ClientController from "./controllers/api/ClientController";
import SessionController from "./controllers/SessionController";
import { restrictWeb, restrict } from "./util/restrictAccess";

const router = Express.Router();

router.get("/api/clients", restrict, ClientController.index);
router.post("/api/clients", restrict, ClientController.create);
router.get("/api/clients/:id", restrict, ClientController.show);
router.patch("/api/clients/:id", restrict, ClientController.update);
router.delete("/api/clients/:id", restrict, ClientController.destroy);
router.get("/sign-out", restrictWeb, SessionController.destroy);
router.use(PageController.index);

export default router;
