import { Model, Document, Schema, model } from "mongoose";
import { ITokenRes } from "../util/lineLogin";

export interface IUserDocument extends Document {
  lineId: string;
  displayName: string;
  profilePicture: string;
}

interface IUserModel extends Model<IUserDocument> {
  findOrCreateByTokenResponse(res: ITokenRes): Promise<IUserDocument | null>;
}

const userSchema = new Schema({
  lineId: { type: String, unique: true },
  displayName: String,
  profilePicture: String
});

userSchema.statics.findOrCreateByTokenResponse = async ({
  id_token
}: ITokenRes): Promise<IUserDocument | null> => {
  const lineId = id_token.sub;
  const user = await User.findOne({ lineId });
  if (user) return user;
  const params = {
    lineId,
    displayName: id_token.name,
    profilePicture: id_token.picture
  };
  return User.create(params);
};

const User: IUserModel = model<IUserDocument, IUserModel>("User", userSchema);

export default User;
