import { Document, Schema, model } from "mongoose";

export interface IClientParams {
  name: string;
  zipcode: string | number;
  city: string;
  area: string;
  address: string;
  phone: string;
  contactPerson: string;
  userId?: Schema.Types.ObjectId;
}

export interface ClientDocument extends Document, IClientParams {}
const clientSchema = new Schema({
  name: {
    type: String,
    required: [true, "請輸入店家名稱。"]
  },
  address: { type: String, required: [true, "請輸入有效的地址。"] },
  phone: {
    type: String,
    required: [true, "請輸入有效的台灣手機號碼。"],
    match: [/^09[0-9]{8}$/, "請輸入有效的台灣手機號碼。"]
  },
  contactPerson: { type: String, required: [true, "請輸入負責人姓名。"] },
  userId: { type: Schema.Types.ObjectId, required: true },
  zipcode: { type: String, required: [true, "請輸入有效的郵遞區號三碼。"] },
  area: { type: String, required: [true, "請輸入地區。"] },
  city: { type: String, required: [true, "請輸入縣市。"] }
});

clientSchema.pre("validate", async function() {
  const doc = this as ClientDocument;
  doc.phone = doc.phone.replace(/[^0-9]/g, "");
});

const Client = model<ClientDocument>("Client", clientSchema);

export default Client;
