import express from "express";
import path from "path";
process.env["NODE_CONFIG_DIR"] = path.resolve(__dirname, "../config");
import logger from "morgan";
import createError from "http-errors";
import helmet from "helmet";
import router from "./router";
import session from "express-session";
import mongoose from "mongoose";
import cookieParser from "cookie-parser";
import fetchUser from "./util/fetchUser";

import LineLogin from "./util/lineLogin";
import config from "config";
import PageController from "./controllers/PageController";

const app = express();
app.use(express.static(path.join(__dirname, "../public")));
app.use(helmet());
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "/views"));
app.use(logger("dev"));
app.use(
  session({
    secret: config.get("session.secret"),
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: config.get("session.maxAge")
    }
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const line = new LineLogin(config.get("line"));
line.applyMiddleware({ app });

app.use(cookieParser());
app.use(fetchUser);
app.use(router);

mongoose
  .connect(config.get("db.mongoUri"), {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    app.listen(3000, () => {
      console.log(`Server listening at http://localhost:3000...`);
    });
  })
  .catch(err => {
    console.log(
      "MongoDB connection error. Please make sure MongoDB is running. " + err
    );
  });
