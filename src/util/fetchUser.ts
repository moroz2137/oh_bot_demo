import { Request, Response } from "express";
import Token from "./token";

function fetchTokenFromCookies(req: Request) {
  return req.cookies.access_token;
}

function fetchBearerToken(req: Request) {
  const header = req.get("Authorization");
  if (!header || !header.match(/^Bearer: /)) {
    return null;
  }
  return header.replace(/^Bearer: /, "");
}

function fetchToken(req: Request) {
  return fetchTokenFromCookies(req) || fetchBearerToken(req);
}

export default async function fetchUser(
  req: Request,
  res: Response,
  next: any
) {
  try {
    res.locals.user = null;
    const token = fetchTokenFromCookies(req) || fetchBearerToken(req);
    const user = await Token.authenticate(token);
    if (user) {
      res.locals.user = user;
    }
  } catch (err) {}
  next();
}
