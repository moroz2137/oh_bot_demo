import { data as zipCodes } from "./zip_codes.json";

export interface Zipcode {
  co: string | number;
  a: string;
  c: string;
}

function filterUnique(value: any, index: number, self: any): boolean {
  return self.indexOf(value) === index;
}

export default class ZipCodeHelper {
  static all() {
    return zipCodes;
  }

  static cities() {
    const allCities = zipCodes.map(d => d.c);
    return allCities.filter(filterUnique);
  }

  static byCity() {
    return zipCodes.reduce((acc, val) => {
      if (!acc[val.c]) acc[val.c] = [val.a];
      else acc[val.c].push(val.a);
      return acc;
    }, {});
  }

  static getAreasByCityName(city: string): string[] {
    return zipCodes.filter(d => d.c === city).map(d => d.a);
  }

  static findByCityArea(city: string, area: string): Zipcode | null {
    const found = zipCodes.find(d => {
      return d.c === city && d.a === area;
    });
    return found || null;
  }

  static isCodeValid(code: number | string): boolean {
    return !!this.findByCode(code);
  }

  static findByCode(code: number | string): Zipcode | null {
    return zipCodes.find(d => d.co == code) || null;
  }
}
