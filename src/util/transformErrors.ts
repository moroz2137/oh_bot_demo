/*
  Returns a concise error object that can be safely returned to
  the client.
*/
export default function transformErrors(response: any) {
  const { errors } = response;
  const transformed = Object.keys(errors).reduce(
    (acc, key) => ({
      ...acc,
      [key]: errors[key]["message"]
    }),
    {}
  );
  return {
    errors: transformed
  };
}
