import config from "config";
import jwt from "jsonwebtoken";
import User, { IUserDocument } from "../models/User";

export default class Token {
  static issue(user: IUserDocument) {
    return jwt.sign(
      {
        sub: user._id
      },
      config.get("session.jwtSigner")
    );
  }

  static authenticate(token: string) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.get("session.jwtSigner"), {}, (err, payload) => {
        if (err) return reject(err);
        const { sub } = payload as any;
        if (!sub) return resolve(null);
        User.findById(sub).then(resolve);
      });
    });
  }
}
