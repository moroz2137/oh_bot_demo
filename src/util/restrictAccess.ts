import { Request, Response } from "express";

export async function restrictWeb(req: Request, res: Response, next: any) {
  if (res.locals.user) next();
  else res.redirect("/login");
}

export async function restrict(req: Request, res: Response, next: any) {
  if (res.locals.user) next();
  else res.status(401).end("Unauthorized");
}
