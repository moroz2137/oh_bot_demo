const lineLogin = require("line-login");
import Express from "express";
import { Request, Response } from "express";
import User from "../models/User";
import Token from "./token";
import config from "config";

interface ILineLoginConfig {
  channelSecret: string;
  channelId: string;
  callbackUrl: string;
  callbackPath: string;
  loginPath: string;
}

export interface ITokenRes {
  id_token: {
    iss: string;
    sub: string;
    aud: string;
    exp: Number;
    iat: Number;
    nonce: string;
    amr: string[];
    name: string;
    picture: string;
  };
}

export default class LineLogin {
  channelId: string;
  channelSecret: string;
  callbackUrl: string;
  callbackPath: string;
  loginPath: string;

  constructor({
    channelId,
    channelSecret,
    callbackUrl,
    loginPath,
    callbackPath
  }: ILineLoginConfig) {
    this.channelId = channelId;
    this.channelSecret = channelSecret;
    this.callbackUrl = callbackUrl;
    this.loginPath = loginPath;
    this.callbackPath = callbackPath;
  }

  get instance() {
    return new lineLogin({
      channel_id: this.channelId,
      channel_secret: this.channelSecret,
      callback_url: this.callbackUrl,
      scope: "openid profile",
      prompt: "consent",
      bot_prompt: "normal"
    });
  }

  get callback() {
    return this.instance.callback(
      LineLogin.successCallback,
      LineLogin.errorCallback
    );
  }

  applyMiddleware({ app }: { app: Express.Application }) {
    app.use(this.loginPath, this.instance.auth());
    app.use(this.callbackPath, this.callback);
  }

  static async successCallback(
    req: Request,
    res: Response,
    next: any,
    tokenRes: ITokenRes
  ) {
    try {
      const user = await User.findOrCreateByTokenResponse(tokenRes);
      if (user) {
        res.cookie("access_token", Token.issue(user), {
          maxAge: config.get("session.maxAge"),
          httpOnly: true
        });
      }
      res.redirect("/");
    } catch (err) {
      next(err);
    }
  }

  static errorCallback(req: Request, res: Response, next: any, error: any) {
    res.status(400).json(error);
  }
}
