import { Response, Request, NextFunction } from "express";
import Client from "../../models/Client";
import transformErrors from "../../util/transformErrors";
import _ from "lodash";

const ALLOWED = [
  "name",
  "address",
  "phone",
  "contactPerson",
  ,
  "zipcode",
  "area",
  "city"
];

export default class ClientController {
  static async index(req: Request, res: Response, next: any) {
    const clients = await Client.find({ userId: res.locals.user._id });
    res.json(clients);
  }

  static async show(req: Request, res: Response, next: NextFunction) {
    const client = await Client.findById(req.params.id);
    if (!client) res.status(404).end("");
    else res.json(client);
  }

  static async create(req: Request, res: Response, next: any) {
    try {
      const client = await new Client({
        ...req.body.client,
        userId: res.locals.user._id
      }).save();
      res.status(201).json(client);
    } catch (error) {
      res.status(422).json(transformErrors(error));
    }
  }

  static async destroy(req: Request, res: Response, next: NextFunction) {
    try {
      const id = req.params.id;
      const user = res.locals.user;
      const client = await Client.findById(req.params.id);
      if (!client) {
        res.status(404).end("Not Found");
        return;
      }
      if ((client as any).userId.equals(user._id)) {
        await Client.deleteOne({ _id: id });
        res.status(204).end("");
      } else {
        res.status(403).end("");
      }
    } catch (error) {
      res.status(500).end("Internal Server Error");
    }
  }

  static async update(req: Request, res: Response, next: any) {
    try {
      const user = res.locals.user;
      const client = await Client.findById(req.params.id);
      if (!client) {
        res.status(404).end("Not Found");
        return;
      }
      if ((client as any).userId.equals(user._id)) {
        ALLOWED.forEach((key: any) => {
          client[key] = req.body.client[key];
        });
        const updated = await client.save();
        res.status(200).json(updated);
      } else {
        res.status(403).end("");
      }
    } catch (error) {
      console.log(error);
      res.status(422).json(transformErrors(error));
    }
  }
}
