import { Request, Response } from "express";
export default class SessionController {
  static destroy(req: Request, res: Response, next: any) {
    res.clearCookie("access_token");
    res.redirect("/");
  }
}
