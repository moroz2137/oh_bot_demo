import { Response, Request } from "express";

export default class PageController {
  static index(req: Request, res: Response, next: any) {
    if (res.locals.user) {
      res.render("index.html.pug");
    } else {
      res.render("landing");
    }
  }
}
