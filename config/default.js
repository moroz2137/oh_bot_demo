module.exports = {
  db: {
    mongoUri: "mongodb://localhost:27017/oh_bot_demo"
  },
  session: {
    maxAge: 12 * 60 * 60 * 1000,
    secret: "5DGJZjCSew9Rl2XNFjWDuChk+9kmedQ7ngtPyBcMrBPXA8nVeXCDqBTooHeb58Xy",
    jwtSigner: "OGkgNQPQGRoT3duNkVjm"
  },
  line: {
    channelSecret: "MY_CHANNEL_SECRET",
    channelId: "MY_CHANNEL_ID",
    loginPath: "/login",
    callbackPath: "/line/callback",
    callbackUrl: "http://localhost:3000/line/callback"
  }
};
