import Clients from "../api/Clients";
import ZipCodeHelper, { Zipcode } from "../../../src/util/ZipCodeHelper";
import Client, { IClientParams } from "../../../src/models/Client";

interface IClientsState {
  all: IClientParams[];
  client: IClientParams | null;
  params: IClientParams;
  loading: boolean;
  id: string | null;
  errors: { [key: string]: string };
}

function newParams() {
  return {
    name: "",
    address: "",
    phone: "",
    contactPerson: "",
    city: "",
    area: "",
    zipcode: ""
  };
}

const state = (): IClientsState => ({
  all: [],
  client: null,
  loading: false,
  id: null,
  params: newParams(),
  errors: {}
});

const actions = {
  async fetchAll({ commit }) {
    const clients = await Clients.fetchAll();
    commit("setClients", clients);
  },
  async fetchOne({ commit }, id) {
    const client = await Clients.fetchOne(id);
    commit("setClient", client);
  },
  async fetchForEdit({ commit }, id) {
    const client = await Clients.fetchOne(id);
    commit("setParams", { client });
  },
  async save({ commit, state }) {
    const res = await Clients.saveClient(state.id, state.params);
    if (res.success) {
      commit("resetParams");
    } else {
      commit("saveFailed", res.errors);
    }
    return res;
  },
  changeParams({ commit, state }, { field, value }) {
    switch (field) {
      case "zipcode":
        const result = ZipCodeHelper.findByCode(value);
        if (result) return commit("changeZipCode", result);
        break;
      case "area":
        const found = ZipCodeHelper.findByCityArea(state.params.city, value);
        if (found) return commit("changeZipCode", found);
        break;
      case "city":
        return commit("changeCity", value);
    }
    commit("changeParams", { field, value });
  }
};

const mutations = {
  setClients(state, clients) {
    state.all = clients;
    state.params = newParams();
  },
  setClient(state, client) {
    state.client = client;
    state.params = newParams();
  },
  setParams(state: IClientsState, { client }) {
    state.params = {
      name: client.name,
      address: client.address,
      zipcode: client.zipcode,
      area: client.area,
      city: client.city,
      phone: client.phone,
      contactPerson: client.contactPerson
    };
    state.errors = {};
    state.id = client._id;
  },
  resetParams(state) {
    state.params = newParams();
    state.errors = {};
  },
  saveFailed(state: IClientsState, errors: any) {
    state.errors = errors;
  },
  changeZipCode(state: IClientsState, { co, a, c }: any) {
    state.params.zipcode = co;
    state.params.city = c;
    state.params.area = a;
  },
  changeCity(state: IClientsState, city: string) {
    state.params.city = city;
    state.params.area = "";
  },
  changeParams(state: IClientsState, { field, value }) {
    state.params[field] = value;
  }
};

export default {
  namespaced: true,
  state: state(),
  actions,
  mutations
};
