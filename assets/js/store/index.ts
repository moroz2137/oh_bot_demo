import Vue from "vue";
import Vuex from "vuex";

import clients from "./clients";

const debug = process.env.NODE_ENV !== "production";

Vue.use(Vuex);
export default new Vuex.Store({
  modules: { clients },
  strict: debug
});
