import Vue from "vue";
import VueRouter from "vue-router";
import ClientList from "../views/ClientList.vue";
import NewClient from "../views/NewClient.vue";
import DisplayClient from "../views/DisplayClient.vue";
Vue.use(VueRouter);

const routes = [
  { path: "/", component: ClientList },
  { path: "/clients/new", component: NewClient },
  { path: "/clients/:id", component: DisplayClient },
  { path: "/clients/:id/edit", component: NewClient }
];

export default new VueRouter({ routes, mode: "history" });
