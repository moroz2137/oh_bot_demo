import "../css/semantic.less";
import "../css/style.sass";
import Vue from "vue";
import store from "./store";
import Main from "./components/Main.vue";

const debug = process.env.NODE_ENV !== "production";
Vue.config.devtools = debug;

document.addEventListener("DOMContentLoaded", () => {
  const container = document.getElementById("app");
  const user = JSON.parse(container.dataset.user);
  console.log(user);
  new Vue({
    render: h => h(Main, { props: { user } }),
    store,
    el: "#app"
  });
});
