import axios from "axios";

import { IClientParams } from "../../../src/models/Client";

interface IMutationResponse {
  success: boolean;
  errors?: { [key: string]: string };
  data?: IClientParams;
}

export default class Clients {
  static async fetchAll(): Promise<IClientParams[]> {
    const { data } = await axios.get("/api/clients", {
      withCredentials: true
    });
    return data;
  }

  static async fetchOne(id): Promise<IClientParams | null> {
    const { data } = await axios.get(`/api/clients/${id}`, {
      withCredentials: true
    });
    return data;
  }

  static async saveClient(id, params): Promise<IMutationResponse> {
    if (id) return this.updateClient(id, params);
    else return this.createClient(params);
  }

  static updateClient(id, client): Promise<IMutationResponse> {
    return this.perform(`/api/clients/${id}`, "PATCH", {
      client
    });
  }

  static createClient(client): Promise<IMutationResponse> {
    return this.perform("/api/clients", "POST", { client });
  }

  static deleteClient(id): Promise<IMutationResponse> {
    return this.perform(`/api/clients/${id}`, "DELETE");
  }

  static async perform(
    url: string,
    method: any = "get",
    data: any = null
  ): Promise<IMutationResponse> {
    try {
      const response = await axios({
        url,
        method,
        data,
        withCredentials: true
      });
      return {
        success: true,
        data: response.data
      };
    } catch (err) {
      return {
        success: false,
        errors: err.response.data.errors
      };
    }
  }
}
